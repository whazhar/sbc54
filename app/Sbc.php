<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Site;

class Sbc extends Model
{
    protected $guarded = [];

    public function sites()
    {
        return $this->hasMany('App\Site');
    }

    public function outpayments()
    {
        return $this->hasMany('App\Outpayment');
    }

    public function customers()
    {
        return $this->hasManyThrough('App\Customer', 'App\Site');
    }

    public function addSite($site)
    {
        Site::create([
            'name' => $site['name'],
            'state' => $site['state'],
            'sbc_id' => $this->id
        ]);
    
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
