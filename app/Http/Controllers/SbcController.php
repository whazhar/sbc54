<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sbc;
use App\Util\Tab;

class SbcController extends Controller
{
    public function index()
    {
        return redirect()->route('sbc.show', Sbc::first());
    }

    public function create()
    {
        return view('sbc.create');
    }

    public function show(Sbc $sbc)
    {
        $this->initTabs($sbc->slug, 'Sites');
        $sbc->sites = $sbc->sites()->paginate(25);
        return view('sbc.show', compact('sbc'));
    }

    public function store()
    {
        Sbc::create([
            'name' => request('name'),
            'shortname' => request('shortname')
            ]);
        
        return redirect()->route('sbc.index'); 
    }

    public function invoices(Sbc $sbc)
    {
        $this->initTabs($sbc->slug, 'Invoices');
        $sbc->outpayments;
        return view('outpayment.index', compact('sbc'));
    }

    private function initTabs($id, $active)
    {
        $tabs = collect([
            'Sites' => Tab::make('Sites', route('sbc.show', $id))->setIcon('fas fa-map-marker-alt'),
            'Invoices' => Tab::make('Invoices', route('invoice.index', $id))->setIcon('fas fa-file-invoice-dollar'),
            'Actions' => Tab::make('', '#')->setIcon('fas fa-plus')->setClass('dropdown'),
        ]);

        $tabs->get('Actions')->submenus = collect([
            Tab::make('Add new Site', route('site.create', $id)),
            Tab::make('Add new Invoice', route('invoice.create', $id)),
        ]);

        $tabs->get($active)->setActive();
        view()->share(compact('tabs'));
    }
}
