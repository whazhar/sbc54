<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sbc;
use App\Site;
use App\Util\Tab;

class SiteController extends Controller
{
    public function index()
    {
        $sites = Site::all();
        return $sites;
    }

    public function show($id)
    {
        $site = Site::where('id', $id)->with('customers', 'outpayments')->first();
        $sbc = $site->sbc;

        $this->initTabs($id, 'Details', $site->customers->count());
        return view('site.show', compact('site', 'sbc'));
    }

    public function create(Sbc $sbc)
    {   
        $states = Site::select('state')->get()->unique('state')->values()->sortBy('state');
        return view('site.create', compact('sbc', 'states'));
    }

    public function store(Request $request, Sbc $sbc)
    {
        $this->validate($request,[
            'name' => 'required',
            'state' => 'required'
        ]);
        
        $site = Site::create(request(['name', 'state']));

        $site->sbc()->associate($sbc)->save();

        $site->histories()->create([
            'event' => 'create site',
            'notes' => 'created this site',
            'username' => 'Anonymous'
        ]);

        return redirect()->route('site.show', $site);
    }

    public function edit(Site $site)
    {
        $this->initTabs($site->id, 'Edit', $site->customers->count());
        $sbc = $site->sbc;
        return view('site.edit', compact('site', 'sbc'));
    }

    public function update(Site $site)
    {
        $site->fill(request()->all());
        $site->save();

        session()->flash('status', "Site $site->name updated!");

        $site->histories()->create([
            'event' => 'update site',
            'notes' => 'updated this site',
            'username' => 'Anonymous'
        ]);

        return redirect()->route('site.show', $site);
    }

    public function customer(Site $site)
    {
        $site->customers;
        $sbc = $site->sbc;
        $this->initTabs($site->id, 'Customers', $site->customers->count());
        
        return view('site.customer', compact('site', 'sbc'));
    }

    public function history(Site $site)
    {
        $this->initTabs($site->id, 'History', $site->customers->count());
        $sbc = $site->sbc;
        $site->histories;
        return view('site.history', compact('site', 'sbc'));
    }

    private function initTabs($id, $active, $count)
    {
        $tabs = collect([
            'Details' => Tab::make('Details', route('site.show', $id))->setIcon('fas fa-info-circle'),
            'Edit' => Tab::make('', route('site.edit', $id))->setIcon('far fa-edit'),
            'Customers' => Tab::make('Customers', route('site.customer', $id))->setIcon('fas fa-user-tie')->setSuffix("&nbsp; <span class=\"badge badge-success bg-success\">$count</span>"),
            'History' => Tab::make('History', route('site.trail', $id))->setIcon('fas fa-history'),
        ]);

        $tabs->get($active)->setActive();
        view()->share(compact('tabs'));
    }
}
