<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Outpayment;
use App\Site;
use App\Sbc;
use App\Util\Tab;

class OutpaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Sbc $sbc)
    {
        $sbc->outpayments;
        
        return view('outpayment.index', compact('sbc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Sbc $sbc)
    {
        $s = $sbc->sites;
        $sites = $s->pluck('id');

        return view('outpayment.create', compact('sbc', 'sites'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sbc $sbc)
    {
        $this->validate($request, [
            'invoice_date' => 'required',
            'amount_in_sen' => 'digit',
            'invoice_no' => 'required',
        ]);

        $outpayment = $sbc->outpayments()->create([
            'invoice_no' => request('invoice_no'),
            'invoice_date' => request('invoice_date'),
            'amount_in_sen' => request('amount_in_sen') * 100
        ]);
        
        $outpayment->sites()->attach(json_decode(request('sites'), true));

        $outpayment->histories()->create([
            'event' => 'create invoice',
            'notes' => 'created this invoice',
            'username' => 'Anonymous'
        ]);

        session()->flash('status', "Invoice $outpayment->invoice_no added to $sbc->name!");

        return redirect()->route('invoice.index', $sbc->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Outpayment $outpayment)
    {
        $this->initTabs($outpayment->id, 'Details', $outpayment->sites->count());
        $outpayment->sites;
        $sbc = $outpayment->sbc;
        return view('outpayment.show', compact('outpayment', 'sbc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Outpayment $outpayment)
    {
        $this->initTabs($outpayment->id, 'Edit', $outpayment->sites->count());
        $sbc = $outpayment->sbc;
        return view('outpayment.edit', compact('outpayment', 'sbc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $outpayment = Outpayment::find($id);
        $outpayment->fill($request->input());
        $outpayment->amount_in_sen = $outpayment->amount_in_sen * 100;
        $outpayment->save();

        $outpayment->histories()->create([
            'event' => 'update invoice',
            'notes' => 'updated this invoice',
            'username' => 'Anonymous'
        ]);

        session()->flash('status', "Invoice $outpayment->name updated!");

        return redirect()->route('invoice.show', $outpayment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sites(Outpayment $outpayment)
    {
        $this->initTabs($outpayment->id, 'Sites', $outpayment->sites->count());
        $sbc = $outpayment->sbc;
        return view('outpayment.sites', compact('outpayment', 'sbc'));
    }

    public function history(Outpayment $outpayment)
    {
        $this->initTabs($outpayment->id, 'History', $outpayment->sites->count());
        $outpayment->histories;
        $sbc = $outpayment->sbc;
        return view('outpayment.history', compact('outpayment', 'sbc'));
    }

    private function initTabs($id, $active, $count)
    {
        $tabs = collect([
            'Details' => Tab::make('Details', route('invoice.show', $id))->setIcon('fas fa-info-circle'),
            'Edit' => Tab::make('', route('invoice.edit', $id))->setIcon('far fa-edit'),
            'Sites' => Tab::make('Sites', route('invoice.sites', $id))->setIcon('fas fa-broadcast-tower')->setSuffix("&nbsp; <span class=\"badge badge-success bg-success\">$count</span>"),
            'History' => Tab::make('History', route('invoice.trail', $id))->setIcon('fas fa-history'),
        ]);

        $tabs->get($active)->setActive();
        view()->share(compact('tabs'));
    }
}
