<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\sitesDataMigration;
use App\Customer;
use App\Sbc;
use App\Site;
use DB;

class ImportData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sbc:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import SBC sites from data migration table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->clearTables();

        sitesDataMigration::all()
            ->groupBy('owner')
            ->each(function ($sites, $sbc){
                $isHeader = $sbc == 'owner';
                if ($isHeader){
                    return;
                }
                
                $sbc = $this->getOwner($sbc);
                $sites->each(function($data) use ($sbc){
                    $site = $this->addSite($sbc, $data);
                $site->histories()->create([
                    'event'=> 'data loading',
                    'notes'=> 'data loaded this site',
                    'username'=> 'System',
                ]);
                });
            });
    }

    public function getOwner($owner)
    {
        return Sbc::firstOrCreate([
            'name' => strtoupper(trim($owner)), 
            'shortName'=>ucfirst(strtolower(trim($owner))),
            'slug' => str_slug($owner, '-')
            ]);
    }

    public function addSite($sbc, $site)
    {
        $s = $sbc->sites()->create([
            'name' => trim($site->site_name),
            'state' => ucwords(strtolower(trim($site->state))),
        ]);
        
        $this->info("Site [{$site->site_name}] has been added to [{$sbc->name}].");

        if ($site->customer != ''){
            $customers = null;
            $customers = explode('/', $site->customer);
        
            foreach ($customers as $customer ){
                $s->customers()->attach($this->addCustomer(ucfirst($customer))->id);
                $this->info("Customer [{$customer}] has been added to [{$site->site_name}].");
            }
        }

        return $s;
    }

    public function addCustomer($customer)
    {
        return Customer::firstOrCreate(['name'=>$customer]);
    }

    public function clearTables()
    {
        Customer::truncate();
        Sbc::truncate();
        Site::truncate();
        DB::table('customer_site')->truncate();
    }
}
