<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\LoadInvoice;
use App\Sbc;
use App\Outpayment;

class LoadDHTI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sbc:dhti';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load DHTI Invoices.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sbc = Sbc::where('name','dhti')->first();
        $invoices = LoadInvoice::all()
                    ->each(function($i)use($sbc){
                        $this->info($i->date_invoice);
                        $sbc->outpayments()->create([
                            'invoice_no' => $i->invoice,
                            'invoice_date' => \Carbon\Carbon::createFromFormat('m/d/Y', $i->date_invoice),
                            'amount_in_sen' => (intval($i->amount) * 100),
                            'shopping_cart_no' => $i->shopping_cart_no,
                            'purchase_order_no' => $i->po_no,
                            'purchase_order_date' => \Carbon\Carbon::createFromFormat('m/d/Y', $i->date_po),
                            'goods_received_no' => $i->gr_no,
                            'goods_received_date' => \Carbon\Carbon::createFromFormat('m/d/Y', $i->date_gr),
                            'pva_no' => $i->pva_no,
                            'remarks' => $i->remarks,
                        ])->histories()->create([
                            'event' => 'data loading',
                            'notes' => 'data loaded this invoice',
                            'username' => 'System',
                        ]);
                        $this->info("Invoice [{$i->invoice}] has been added to [{$sbc->name}].");
                    });


    }
}
