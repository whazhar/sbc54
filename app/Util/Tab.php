<?php

namespace App\Util;

class Tab
{
    private $_label;
    public $class = '';
    public $bgColor = '';

    public static function make($label, $url, $isActive = false)
    {
        return new self($label, $url, $isActive);
    }

    public function __construct($label, $url, $isActive = false)
    {
        $this->label = $this->_label = $label;
        $this->url = $url;
        $this->isActive = $isActive;
    }

    public function setIcon($icon)
    {
        $this->label = $this->_label = "<i class=\"$icon\"></i>&nbsp; " . $this->_label;
        return $this;
    }

    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    public function setSuffix($suffix)
    {
        $this->label = $this->_label = $this->_label . $suffix;
        return $this;
    }

    public function setBgColor($color)
    {
        $this->bgColor = $color;
        return $this;
    }

    public function setActive()
    {
        $this->isActive = true;
        return $this;
    }
}
