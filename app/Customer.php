<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];

    public function sites()
    {
        return $this->belongsToMany('App\Site');
    }
}
