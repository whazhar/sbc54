<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outpayment extends Model
{
    protected $guarded = [];

    public function sites()
    {
        return $this->belongsToMany('App\Site');
    }

    public function sbc()
    {
        return $this->belongsTo('App\Sbc');
    }

    public function histories()
    {
        return $this->morphMany('App\History', 'logger');
    }

    public function month()
    {
        $d = \Carbon\Carbon::createFromFormat('Y-n-j',$this->invoice_date);
        return $d->format('F'). ' ' . $d->format('Y');
    }

    public function price()
    {
        return $this->amount_in_sen / 100;
    }
}
