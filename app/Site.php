<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $guarded = [];

    public function sbc()
    {
        return $this->belongsTo('App\Sbc');
    }

    public function customers()
    {
        return $this->belongsToMany('App\Customer');
    }

    public function outpayments()
    {
        return $this->belongsToMany('App\Outpayment');
    }

    public function histories()
    {
        return $this->morphMany('App\History', 'logger');
    }

    // public function addOutpayment($outpayment)
    // {
    //     Outpayment::create([
    //         'site_id' => $this->id,
    //         'invoice_no' => $outpayment['invoice_no']
    //     ]);
    // }
}
