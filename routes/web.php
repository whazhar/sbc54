<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('sbc.index');
});

Route::get('/sbc', 'SbcController@index')->name('sbc.index');
Route::get('/sbc/create', 'SbcController@create')->name('sbc.create');
Route::get('/sbc/{sbc}', 'SbcController@show')->name('sbc.show');
Route::post('/sbc', 'SbcController@store')->name('sbc.store');
Route::get('/sbc/{sbc}/invoices/', 'SbcController@invoices')->name('invoice.index');

Route::get('/site', 'SiteController@index')->name('site.index');
Route::get('/site/create/{sbc}', 'SiteController@create')->name('site.create');
Route::get('/site/{site}/edit', 'SiteController@edit')->name('site.edit');
Route::get('/site/{id}', 'SiteController@show')->name('site.show');
Route::get('/site/{site}/trail', 'SiteController@history')->name('site.trail');
Route::get('/site/{site}/customer', 'SiteController@customer')->name('site.customer');
Route::post('/sbc/{sbc}/site', 'SiteController@store')->name('site.store');
Route::patch('/site/{site}', 'SiteController@update')->name('site.update');
Route::delete('/site/{site}', 'SiteController@destroy')->name('site.delete');

Route::get('/customer', 'CustomerController@index')->name('customer.index');
Route::get('/customer/{id}', 'CustomerController@show')->name('customer.show');
Route::post('/customer', 'CustomerController@store')->name('customer.store');
Route::post('/site/{id}/subscribe', 'CustomerController@subscribe')->name('customer.subscribe');

Route::get('/invoices/create/{sbc}', 'OutpaymentController@create')->name('invoice.create');
Route::get('/invoices/{outpayment}', 'OutpaymentController@show')->name('invoice.show');
Route::get('/invoices/{outpayment}/edit', 'OutpaymentController@edit')->name('invoice.edit');
Route::get('/invoices/{outpayment}/sites', 'OutpaymentController@sites')->name('invoice.sites');
Route::post('/invoices/{sbc}', 'OutpaymentController@store')->name('invoice.store');
Route::patch('/invoices/{id}', 'OutpaymentController@update')->name('invoice.update');
Route::get('/invoices/{outpayment}/trail', 'OutpaymentController@history')->name('invoice.trail');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
