@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $sbc->slug)}}">{{$sbc->name}}</a></li>
            <li class="breadcrumb-item" aria-current="page">Sites</li>
            <li class="breadcrumb-item active" aria-current="page">New site</li>
        </ol>
    </nav>
</div>    
@endsection

@section('content')
<div class="row pb-2 border-bottom">
    <div class="col">
        <h2>{{ $sbc->name }}</h2>
        <h4>New Site</h4>    
    </div>
</div>

@if ($errors->any())
<div class="row">
    <div class="col">
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif

<div class="row py-4">
    
    <div class="col-md-6 order-md-1 mb-2">
        <form action="{{route('site.store', $sbc)}}" method="POST">
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="name" class="col-sm-3 col-form-label">Site Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>

            <div class="form-group row">
                <label for="state" class="col-sm-3 col-form-label">State</label>
                <div class="col-sm-9">
                    <select class="form-control form-control-lg" id="state" name="state">
                        @foreach($states as $state)    
                            <option>{{$state->state}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Create site</button>
                </div>
            </div>
        </form>
        </div>
</div>

@endsection