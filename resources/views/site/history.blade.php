@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $site->sbc->slug)}}">{{$site->sbc->name}}</a></li>
            <li class="breadcrumb-item" aria-current="page">Sites</li>
            <li class="breadcrumb-item"><a href="{{route('site.show', $site->id)}}">{{$site->name}}</a></li>
            <li class="breadcrumb-item active">Trail</li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
    @include('tabs')
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h3>{{ $site->sbc->name }}</h3>
        <h4>Trails</h4>
    </div>
</div>

<div class="row">
    <div class="col">
            <table class="table table-striped table-sm">
                <tbody>
                @if(!$site->histories->isEmpty())
                    @foreach($site->histories as $history)
                        <tr>
                            <td>{{$history->username}} {{$history->notes}} {{$site->name}} on {{$history->updated_at}}.</td>
                        </tr>
                    @endforeach
                @else
                    <tr><td>28/02/2018</td><td>Son Goku updated this site 3 years ago!</td></tr>
                @endif
                </tbody>
            </table>
    </div>
</div>
@endsection