@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $site->sbc->name)}}">{{$site->sbc->name}}</a></li>
            <li class="breadcrumb-item" aria-current="page">Sites</li>
            <li class="breadcrumb-item"><a href="{{route('site.show', $site->id)}}">{{$site->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit</li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
    @include('tabs')
@endsection

@section('content')
<form action="{{route('site.update', $site)}}" method="POST">
    <div class="form-group row">
        <div class="col-sm-6">
            <h3>{{ $site->sbc->name }}</h3>
            <h4><input type="text" class="form-control" name="name" value="{{$site->name}}"></h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 order-md-1 mb-2">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="form-group row">
                <label for="address" class="col-sm-3 col-form-label">Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="address" name="address" value="{{$site->address}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="awo_id" class="col-sm-3 col-form-label">AWO Id</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="awo_id" name="awo_id" value="{{$site->awo_id}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="contract_period" class="col-sm-3 col-form-label">Contract Period</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="contract_period" name="contract_period" value="{{$site->contract_period}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="price" class="col-sm-3 col-form-label">Default Price</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="price" name="price" value="{{$site->price}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="bandwidth" class="col-sm-3 col-form-label">Bandwidth</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="bandwidth" name="bandwidth" value="{{$site->bandwidth}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="total_core" class="col-sm-3 col-form-label">Total Core</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="total_core" name="total_core" value="{{$site->total_core}}">
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Update site</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection