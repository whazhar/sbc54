@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $site->sbc->slug)}}">{{$site->sbc->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sites</li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('site.show', $site->id)}}">{{$site->name}}</a></li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
@include('tabs')
@endsection

@section('actions')
    @include('util.alert')
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h3>{{ $site->sbc->name }}</h3>
        <h4>{{ $site->name }}</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12 order-md-9 mb-9">
        <div class="card">
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-3">Address</dt>
                    <dd class="col-sm-3">{{$site->address}}</dd>
                    <dt class="col-sm-3">State</dt>
                    <dd class="col-sm-3">{{$site->state}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">Coordinates</dt>
                    <dd class="col-sm-3">
                        {{$site->latitude == null && $site->longitude == null ? '&nbsp;' : $site->latitude . ',' . $site->longitude}}
                    </dd>
                    <dt class="col-sm-3">AWO Id</dt>
                    <dd class="col-sm-3">{{$site->awo_id}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">Start Date</dt>
                    <dd class="col-sm-3">{{$site->start_date}}</dd>
                    <dt class="col-sm-3">End Date</dt>
                    <dd class="col-sm-3">{{$site->end_date}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">Contract Period</dt>
                    <dd class="col-sm-3">{{$site->contract_period}}</dd>
                    <dt class="col-sm-3">Price</dt>
                    <dd class="col-sm-3">{{$site->price}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">Bandwidth</dt>
                    <dd class="col-sm-3">{{$site->bandwidth}}</dd>
                    <dt class="col-sm-3">Total Core</dt>
                    <dd class="col-sm-3">{{$site->total_core}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">Available Core</dt>
                    <dd class="col-sm-3">{{$site->current_available_core}}</dd>
                    <dt class="col-sm-3">Subscribed</dt>
                    <dd class="col-sm-3">{{$site->subscribed}}</dd>
                </dl>
            </div>
        </div>

        <br />
        <h4>Invoices</h4>
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Invoice #</th>
                <th>Date</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            @foreach($site->outpayments as $outpayment)
                <tr>
                    <td><a href="{{route('invoice.show',$outpayment->id)}}">{{ $outpayment->invoice_no }}</a></td>
                    <td>{{ $outpayment->invoice_date }}</td>
                    <td>RM {{ $outpayment->price() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>



@endsection