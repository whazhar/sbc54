@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $site->sbc->slug)}}">{{$site->sbc->name}}</a></li>
            <li class="breadcrumb-item" aria-current="page">Sites</li>
            <li class="breadcrumb-item"><a href="{{route('site.show', $site->id)}}">{{$site->name}}</a></li>
            <li class="breadcrumb-item active">Customers</li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
    @include('tabs')
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h3>{{ $site->sbc->name }}</h3>
        <h4>Customers</h4>
    </div>
</div>

<div class="row">
    <div class="col">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                @if(!$site->customers->isEmpty())
                    @foreach($site->customers as $customer)
                        <tr>
                            {{-- <td><a href="{{route('customer.show',$customer->id)}}">{{ $customer->name }}</a></td> --}}
                            <td>{{ $customer->name }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
    </div>
</div>
@endsection
