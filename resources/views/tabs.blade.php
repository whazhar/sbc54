<div class="container">
    <ul class="nav nav-tabs">
        @if(isset($tabs))
            @foreach($tabs as $tab)
                @if(!$tab->class == 'dropdown')
                <li class="nav-item {{$tab->class}}">
                    <a href="{{$tab->url}}" class="nav-link {{$tab->isActive ? "active" : ""}}">
                        {!! $tab->label !!}
                    </a>
                </li>
                @else
                <li class="nav-item {{$tab->class}}">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                         {!! $tab->label !!}
                    </a>
                    <div class="dropdown-menu">
                        @foreach ($tab->submenus as $menu)
                            <a class="dropdown-item" href="{{$menu->url}}">{{$menu->label}}</a>
                        @endforeach
                    </div>
                </li>
                @endif
            @endforeach
        @endif
    </ul>
</div>