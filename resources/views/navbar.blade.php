<nav class="navbar navbar-expand-sm navbar-light bg-light mb-0 p-0">
    <a href="/" class="navbar-brand mr-4 px-4 text-secondary font-weight-bold"><i class="fas fa-broadcast-tower"></i>&nbsp;&nbsp;SBC</a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#SBCNavDropdown" aria-controls="SBCNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="SBCNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="SBCDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if (isset($sbc))
                        {{$sbc->name}}
                    @else
                        SBC List
                    @endif
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    @foreach($nav as $n)
                        <a class="dropdown-item" href="{{ route('sbc.show',$n->slug) }}">{{ $n->name }}</a>
                    @endforeach
                </div>
            </li>
        </ul>

        <ul class="navbar-nav float-sm-right">
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" role="button" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wan Azhar</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdownMenuLink">
                    <a href="#" class="dropdown-item">Logout</a>
                </div>
            </li>
        </ul>
    </div>
{{--     
    <div class="navbar-nav mr-4">
        <form class="form-inline">
            <input class="form-control zform-text" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-link p-0" type="submit"><i class="fas fa-search btn"></i></button>
        </form>
    </div>
     --}}
{{-- 
    Welcome, <span class="font-weight-bold">Wan</span>
<div class="mr-2">&nbsp;</div> --}}


</nav>

