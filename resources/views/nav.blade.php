<div class="col-md-3 order-md-2 mb-3">
    <ul class="list-group mb-3">
        @foreach($nav as $navitem)
            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <div>
                    <span class="my-0"><a href="{{route('sbc.show', $navitem->name)}}">{{ $navitem->name }}</a></span>
                    <small class="text-muted">{{ $navitem->shortname }}</small>
                </div>
            </li>
        @endforeach
    </ul>

    @if (!Request::is('create/sbc'))
        <div class="card">
            <a class="btn btn-outline-primary btn-sm" href="{{route('sbc.create')}}">Add SBC</a>
        </div>
    @endif
</div>