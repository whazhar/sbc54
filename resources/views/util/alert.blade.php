
@if($flash = session('status'))
<div class="alert alert-success fixed-top text-center" style="display:none" id="alert" role="alert">
    {{$flash}}
</div>
@section('scripts')
<script type="text/javascript">
    $(function() {
        $('#alert').slideDown('slow').delay(1000).slideUp('slow');
    });
    </script>
@endsection
@endif