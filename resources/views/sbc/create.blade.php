@extends('layout')

@section('content')
<div class="row">
    @include('nav')

    <div class="col-md-6 order-md-2 mb-2">
        <h2>Add new SBC ::</h2>
        <form action="{{route('sbc.store')}}" method="POST">
            {{ csrf_field() }}
            <div class="card p-4">
                <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label col-form-label-sm">Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="name" name="name" placeholder="Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="shortname" class="col-sm-3 col-form-label col-form-label-sm">Short Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="shortname" name="shortname" placeholder="Short Name">
                    </div>
                </div>
                <button class="btn btn-primary btn-sm mt-3" type="submit">Add new SBC</button>
            </div>
        </form>
    </div>
</div>

@endsection