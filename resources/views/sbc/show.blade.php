@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $sbc->slug)}}">{{$sbc->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Sites</li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
    @include('tabs')
@endsection


@section('content')
<div class="row">
    
    <div class="col-md-12 order-md-12 mb-12">
        <div class="table-responsive">
            {{-- <h2>{{ $sbc->name }}</h2> --}}
{{-- 
            <div>
                <a class="btn btn-outline-primary btn-sm" href="{{route('outpayment.create', ['id' => $sbc->id])}}">Add Invoice</a>
                <a class="btn btn-outline-primary btn-sm" href="{{route('site.create', ['id' => $sbc->id])}}">Add Site</a>
            </div> --}}

            <table class="table table-sm">
                <thead>
                <tr>
                    <th>Site</th>
                    <th>State</th>
                    <th>Address</th>
                    {{-- <th>Coordinates</th> --}}
                    <th>Start-End</th>
                    <th>Available / Total Core</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sbc->sites as $site)
                    <tr>
                        <td><a href="/site/{{ $site->id }}" target="_blank">{{ $site->name }}</a></td>
                        <td>{{ $site->state }}</td>
                        <td>{{ $site->address }}</td>
                        {{-- <td>{{ $site->latitude != null && $site->latitude != null ? $site->latitude . ',' . $site->longitude : '&nbsp;' }}</td> --}}
                        <td>{{ $site->start_date != null && $site->end_date != null ? $site->start_date . '-' . $site->end_date : '&nbsp;' }}</td>
                        <td>{{ !$site->current_available_core == null ? $site->current_available_core . '/' . $site->total_core : '&nbsp;' }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-end">{{ $sbc->sites->links() }}</div>
        </div>
    </div>
</div>



@endsection