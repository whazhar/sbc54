@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $sbc->slug)}}">{{$sbc->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Invoices</li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('invoice.show', $outpayment->id)}}">{{$outpayment->invoice_no}}</a></li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
    @include('tabs')
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h2>{{ $outpayment->invoice_no }}</h2>
        <h4>RM {{$outpayment->price()}}</h4>
        <h4>{{$outpayment->month()}}</h4>        
    </div>
</div>
<div class="row mt-4">

    <div class="col-md-6 order-md-6 mb-6">
        <div class="card">
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6">Shopping Cart #</dt>
                    <dd class="col-sm-3">{{$outpayment->shopping_cart_no}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">Purchase Order #</dt>
                    <dd class="col-sm-3">{{$outpayment->purchase_order_no}}</dd>
                    <dt class="col-sm-3">Purchase Order Date</dt>
                    <dd class="col-sm-3">{{$outpayment->purchase_order_date}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">Goods Received #</dt>
                    <dd class="col-sm-3">{{$outpayment->goods_receive_no}}</dd>
                    <dt class="col-sm-3">Goods Received Date</dt>
                    <dd class="col-sm-3">{{$outpayment->goods_received_date}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">PVA #</dt>
                    <dd class="col-sm-3">{{$outpayment->pva_no}}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-sm-3">Invoice recurring</dt>
                    <dd class="col-sm-3">{{$outpayment->recurring}}</dd>
                </dl>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col"><small class="text-muted">Last Updated: {{$outpayment->updated_at}}</small></div>
</div>


@endsection