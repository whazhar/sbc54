@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $sbc->name)}}">{{$sbc->name}}</a></li>
            <li class="breadcrumb-item" aria-current="page">Invoices</li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('invoice.show', $outpayment->id)}}">{{$outpayment->invoice_no}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit</li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
    @include('tabs')
@endsection

@section('content')
<form action="{{route('invoice.update', $outpayment)}}" method="POST">
    <div class="form-group row">
        <div class="col-sm-6">
            <h2><input type="text" class="form-control" name="invoice_no" value="{{ $outpayment->invoice_no }}"></h2>
            <h4>RM <input type="text" class="form-control" style="display:inline; width:auto" name="amount_in_sen" value="{{ $outpayment->price() }}"></h4>
            <h4><input type="date" class="form-control" name="invoice_date" value="{{ $outpayment->invoice_date }}"></h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 order-md-1 mb-2">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="form-group row">
                <label for="shopping_cart_no" class="col-sm-3 col-form-label">Shopping Cart #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="shopping_cart_no" name="shopping_cart_no" value="{{$outpayment->shopping_cart_no}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="purchase_order_no" class="col-sm-3 col-form-label">Purchase Order #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="purchase_order_no" name="purchase_order_no" value="{{$outpayment->purchase_order_no}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="purchase_order_date" class="col-sm-3 col-form-label">Purchase Order Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="purchase_order_date" name="purchase_order_date" value="{{$outpayment->purchase_order_date}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="goods_received_no" class="col-sm-3 col-form-label">Goods Received #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="goods_received_no" name="goods_received_no" value="{{$outpayment->goods_received_no}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="goods_received_date" class="col-sm-3 col-form-label">Goods Received Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="goods_received_date" name="goods_received_date" value="{{$outpayment->goods_received_date}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="pva_no" class="col-sm-3 col-form-label">PVA #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="pva_no" name="pva_no" value="{{$outpayment->pva_no}}">
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Update invoice</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection