@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $sbc->slug)}}">{{$sbc->name}}</a></li>
            <li class="breadcrumb-item" aria-current="page">Invoices</li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{route('invoice.show', $outpayment->id)}}">{{$outpayment->invoice_no}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit</li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
    @include('tabs')
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <h2>{{ $outpayment->invoice_no }}</h2>
        <h4>{{$outpayment->month()}}</h4>        
    </div>
</div>

<div class="row">
    <div class="col">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>State</th>
                    </tr>
                </thead>
                <tbody>
                @if(!$outpayment->sites->isEmpty())
                    @foreach($outpayment->sites as $site)
                        <tr>
                            <td><a href="{{route('site.show',$site->id)}}">{{ $site->name }}</a></td>
                            <td>{{ $site->state }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
    </div>
</div>
@endsection