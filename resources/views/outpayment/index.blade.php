@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('sbc.show', $sbc->slug)}}">{{$sbc->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Invoices</li>
        </ol>
    </nav>
</div>    
@endsection

@section('subnav')
    @include('tabs')
@endsection

@section('actions')
    @include('util.alert')
@endsection

@section('content')
<div class="row">
    
    <div class="col-md-12 order-md-12 mb-12">
        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th>Invoice #</th>
                    <th>Invoice Date</th>
                    <th>Sites</th>
                    {{-- <th>Status</th> --}}
                    <th>Last updated</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sbc->outpayments as $outpayment)
                    <tr>
                        <td><a href="{{ route('invoice.show',$outpayment->id) }}" target="_blank">{{ $outpayment->invoice_no }}</a></td>
                        <td>{{ $outpayment->invoice_date }}</td>
                        <td>{{ $outpayment->sites->count() }}</td>
                        {{-- <td>{{ $outpayment->updated_at }}</td> --}}
                        <td>{{ $outpayment->updated_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{-- <div class="d-flex justify-content-end">{{ $sbc->outpayments->links() }}</div> --}}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
            $(function() {
                $('#alert').slideDown('slow').delay(1500).slideUp('slow');
            });
    </script>
@endsection