@extends('layout')

@section('breadcrumbs')
<div class="container my-2">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('invoice.index', $sbc->slug)}}">{{$sbc->name}}</a></li>
            <li class="breadcrumb-item" aria-current="page">Invoices</li>
            <li class="breadcrumb-item active" aria-current="page">New invoice</li>
        </ol>
    </nav>
</div>    
@endsection

@section('content')
<div class="row pb-2 border-bottom">
    <div class="col">
        <h2>{{ $sbc->name }}</h2>
        <h4>New Invoice</h4>    
    </div>
</div>

@if ($errors->any())
<div class="row">
    <div class="col">
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif

<div class="row py-4">
    <div class="col-md-6 order-md-1 mb-2">
        <form action="{{route('invoice.store', $sbc)}}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="sites" value="{{$sites}}">
            <div class="form-group row">
                <label for="sbc" class="col-sm-3 col-form-label">Invoice for</label>
                <div class="col-sm-9">
                    <input type="text" readonly class="form-control-plaintext" id="sbc" name="sbc" value="{{$sbc->name}}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="sites" class="col-sm-3 col-form-label">Number of sites</label>
                <div class="col-sm-9">
                    {{$sbc->sites->count()}} sites
                </div>
            </div>

            <div class="form-group row">
                <label for="invoice_no" class="col-sm-3 col-form-label">Invoice #</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="invoice_no" name="invoice_no" placeholder="Invoice #" value="{{old('invoice_no')}}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="invoice_date" class="col-sm-3 col-form-label">Date</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="invoice_date" name="invoice_date" placeholder="Date" value="{{old('invoice_date')}}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="amount_in_sen" class="col-sm-3 col-form-label">Invoice Amount (RM)</label>
                <div class="col-sm-9">
                    <input type="integer" class="form-control" id="amount_in_sen" name="amount_in_sen" placeholder="Amount" value="{{old('amount_in_sen')}}">
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Create invoice</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-6 order-md-2 mb-2">
        <div class="card bg-light mb-3">
                <div class="card-body">
                    <h5 class="card-title">This invoice covers the sites below:</h5>
                    <ul>
                        @foreach($sbc->sites as $site)    
                            <li>{{$site->name}}</li>
                        @endforeach
                    </ul>
                </div>
        </div>
    </div>
</div>

@endsection
