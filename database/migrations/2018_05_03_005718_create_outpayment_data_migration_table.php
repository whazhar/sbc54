<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutpaymentDataMigrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outpayment_data_migration', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('invoice');
            $table->string('date_invoice');
            $table->string('site');
            $table->integer('amount');
            $table->string('owner');
            $table->string('state');
            $table->string('shopping_cart_no');
            $table->string('po_no');
            $table->string('date_po');
            $table->string('gr_no');
            $table->string('date_gr');
            $table->string('pva_no');
            $table->string('date_pva');
            $table->string('remarks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outpayment_data_migration');
    }
}
