<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutpaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outpayments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('invoice_no');
            $table->integer('amount_in_sen');
            $table->date('invoice_date');
            $table->string('shopping_cart_no')->nullable();
            $table->string('purchase_order_no')->nullable();
            $table->date('purchase_order_date')->nullable();
            $table->string('goods_received_no')->nullable();
            $table->date('goods_received_date')->nullable();
            $table->string('pva_no')->nullable();
            $table->binary('recurring')->nullable();
            $table->string('remarks')->nullable();
            $table->integer('sbc_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outpayments');
    }
}
